import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public List<Item> getAllItems() {
        log.info("Finding items");
        return itemRepository.findAll();
    }

    @Override
    public Item getItemById(Long id) {
        log.info("Getting item with id {}", id);
        return itemRepository.findById(id).orElse(null);
    }

    @Override
    public Item createItem(Item item) {
        log.info("Creating new Item");
        // Additional business logic can be added here
        return itemRepository.save(item);
    }

    @Override
    public Item updateItem(Long id, Item item) {
        // Additional business logic can be added here
        log.info("Updating new Item");
        Item existingItem = getItemById(id);
        if (existingItem != null) {
            existingItem.setTitle(item.getTitle());
            existingItem.setDescription(item.getDescription());
            return itemRepository.save(existingItem);
        }
        return null;
    }

    @Override
    public void deleteItem(Long id) {
        log.info("Deleting item with id {}", id);
        // Additional business logic can be added here
        itemRepository.deleteById(id);
    }

}
