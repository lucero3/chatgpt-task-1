import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ItemServiceTest {

    @Mock
    private ItemRepository itemRepository;

    @InjectMocks
    private ItemServiceImpl itemService;

    @Test
    public void testGetAllItems() {
        // Arrange
        Item item1 = new Item("Task 1", "Description 1");
        Item item2 = new Item("Task 2", "Description 2");
        List<Item> items = Arrays.asList(item1, item2);
        when(itemRepository.findAll()).thenReturn(items);

        // Act
        List<Item> result = itemService.getAllItems();

        // Assert
        assertEquals(2, result.size());
        assertEquals(item1.getTitle(), result.get(0).getTitle());
        assertEquals(item2.getDescription(), result.get(1).getDescription());
    }

    @Test
    public void testGetItemById() {
        // Arrange
        Long itemId = 1L;
        Item item = new Item("Task 1", "Description 1");
        when(itemRepository.findById(itemId)).thenReturn(Optional.of(item));

        // Act
        Item result = itemService.getItemById(itemId);

        // Assert
        assertNotNull(result);
        assertEquals("Task 1", result.getTitle());
    }

    @Test
    public void testCreateItem() {
        // Arrange
        Item itemToCreate = new Item("Task 1", "Description 1");
        when(itemRepository.save(itemToCreate)).thenReturn(itemToCreate);

        // Act
        Item createdItem = itemService.createItem(itemToCreate);

        // Assert
        assertNotNull(createdItem);
        assertEquals("Task 1", createdItem.getTitle());
        assertEquals("Description 1", createdItem.getDescription());
    }

    @Test
    public void testUpdateItem() {
        // Arrange
        Long itemId = 1L;
        Item existingItem = new Item("Task 1", "Description 1");
        when(itemRepository.findById(itemId)).thenReturn(Optional.of(existingItem));

        Item updatedItem = new Item("Updated Task", "Updated Description");

        // Act
        Item result = itemService.updateItem(itemId, updatedItem);

        // Assert
        assertNotNull(result);
        assertEquals("Updated Task", result.getTitle());
        assertEquals("Updated Description", result.getDescription());
    }

    @Test
    public void testDeleteItem() {
        // Arrange
        Long itemId = 1L;

        // Act
        itemService.deleteItem(itemId);

        // Assert
        verify(itemRepository, times(1)).deleteById(itemId);
    }
}
