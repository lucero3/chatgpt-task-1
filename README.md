# Todo Application

This is a simple Todo application built with Spring Boot. It provides REST APIs for managing todo items (or items) with functionalities to create, read, update, and delete items. Each item has a title and a description.

## Technologies Used

- Java
- Spring Boot
- Spring Data JPA
- Hibernate ORM
- MySQL Database
- JUnit 5
- Mockito
- JaCoCo (for test coverage)
- SonarQube (for code quality metrics)

## How to Run

1. **Clone the Repository**:
git clone https://gitlab.com/lucero3/chatgpt-task-1

2. **Configure MySQL Database**:

- Create a MySQL database named `todo_db`.
- Update the database connection properties in `src/main/resources/application.properties`:
  ```properties
  spring.datasource.url=jdbc:mysql://localhost:3306/todo_db
  spring.datasource.username=root
  spring.datasource.password=your_mysql_password
  ```

3. **Build the Application**:

Navigate to the project directory and run the following Maven command:

mvn clean install


4. **Run the Application**:

After the build is successful, run the application using the following command:

mvn spring-boot:run


5. **Access the API Endpoints**:

Once the application is running, you can access the API endpoints at `http://localhost:8080/items`.

6. **Run Tests and Generate Reports**:

To run tests and generate test coverage reports, execute:

mvn clean verify

You can find the coverage reports in the `target/site/jacoco` directory.

7. **Analyze Code Quality with SonarQube**:

- Start your SonarQube server.
- Execute the following Maven command to analyze code quality:
  ```
  mvn sonar:sonar \
    -Dsonar.host.url=http://localhost:9000 \
    -Dsonar.login=<your-sonarqube-token>
  ```
  Replace `<your-sonarqube-token>` with your SonarQube authentication token.

## API Documentation

- **GET /items**: Get all todo items.
- **GET /items/{id}**: Get a todo item by ID.
- **POST /items**: Create a new todo item.
- **PUT /items/{id}**: Update an existing todo item.
- **DELETE /items/{id}**: Delete a todo item by ID.

## Contribution

Contributions are welcome! If you find any issues or have suggestions for improvement, feel free to open an issue or submit a pull request.

## Questions about the used of AI to build the application

- Was it easy to complete the task using AI? 
It was very easy. I just made a few changes. But in general the outputs were good enough.

- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics) 
It took me around 3 hours to complete this specific task

- Was the code ready to run after generation? What did you have to change to make it usable?
Yes
- Which challenges did you face during completion of the task?
Just thinking about the right prompt to accomplish the task effectively
- Which specific prompts you learned as a good practice to complete the task?
This one was the most useful:

I would like to develop a Java application using Spring Boot, which provides REST API and uses MySQL database and hibernate ORM.  The application should allow users to create, read, update, and delete todo items. Each item should have a title and a description. Use Hibernate to persist the items in the database.
Please provide a set of technologies and frameworks required to develop such an application. 
Create a list of tasks with examples of prompts I can ask you for each task to get relevant examples. 



